<?php namespace Garretfick\Trustedproxies;

use System\Classes\PluginBase;

/**
 * trustedproxy Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'october-trustedproxies',
            'description' => 'Use fideloper/TrustedProxy with OctoberCMS',
            'author'      => 'Garret Fick',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
        $this->app['Illuminate\Contracts\Http\Kernel']
            ->pushMiddleware('Garretfick\Trustedproxies\Middleware\TrustedProxy');
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return [
            'garretfick.trustedproxies.settings_write' => [
                'tab' => 'Trusted Proxies',
                'label' => 'Manage Trusted Proxies'
            ],
        ];
    }

    public function registerSettings()
    {
        return [
            'settings' => [
                'label'       => 'Trusted Proxies',
                'description' => 'Manage Trusted Proxies.',
                'icon'        => 'icon-globe',
                'class'       => 'Garretfick\Trustedproxies\Models\Settings',
                'order'       => 100,
                'category'    => 'System',
                'keywords'    => 'trust proxy proxies load balancer',
                'permissions' => ['garretfick.trustedproxies.settings_write']
            ]
        ];
    }
}
