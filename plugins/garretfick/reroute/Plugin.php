<?php namespace Garretfick\Reroute;

use Backend;
use System\Classes\PluginBase;

/**
 * reroute Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'reroute',
            'description' => 'Reroute URLs. This is how I mapped the old website URLs to the new structure',
            'author'      => 'garretfick',
            'icon'        => 'icon-leaf'
        ];
    }
}
