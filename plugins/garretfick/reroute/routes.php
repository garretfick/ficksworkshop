<?php

Route::get('blog/{category}/{post}', function($category, $post) {
    // Extract the name from the poat part of the URL
    $start = strpos($post, '-');
    $newSlug = substr($post, $start + 1);
    return Redirect::to('/blog/post/' . $newSlug, 301); 
})->where(['category' => '[0-9]{1,3}-[\w\-]+', 'post' => '[0-9]{1,3}-[\w\-]+']);
